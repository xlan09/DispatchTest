'''
examples of how to use world.py
'''

from world import World
from time import sleep

'''
static obstacle world
drive until collision or 100 steps
'''

# init world
world = World()
# populate obstacle field with static obstacles
world.generate_obstacles(static=True)
# init pygame screen for visualization
screen = world.init_screen()
# get vehicle
vehicle = world.get_vehicle()
# set vehicle velocity
linear_velocity = 0.5
angular_velocity = 0.25
vehicle.set_commands(
    linear_velocity,
    angular_velocity
)

# timestep for world update
dt = 0.1

print 'Running static obstacles world'
for _ in xrange(0, 100):
    # collision testing
    if world.in_collision():
        print 'Collision'
        break
    # update world
    world.update(dt)
    # draw world
    world.draw(screen)
    # wait dt amount of time
    sleep(dt)

# provide some time to view result
sleep(2.0)
