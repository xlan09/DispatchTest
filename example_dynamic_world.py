'''
examples of how to use world.py
'''

from world import World
import numpy as np
from time import sleep
from motion_planner import MotionPlanner

'''
dynamic obstacle world
drive until collision or goal position reached
'''

# init world
start_pose = np.array([1.0, 1.0, np.pi / 4.0])
goal = np.array([4.0, 4.0])
goal_tolerance = 0.25

world = World(
    start_pose=start_pose
)
# populate obstacle field with dynamic obstacles
world.generate_obstacles(n_obstacles=20, static=False)
# init pygame screen for visualization
screen = world.init_screen()
# get vehicle
vehicle = world.get_vehicle()
# set vehicle velocity
linear_velocity = 0.5
angular_velocity = 0
vehicle.set_commands(
    linear_velocity,
    angular_velocity
)

# timestep for world update
dt = 0.1

# initialize planner
max_linear_velocity = 1
max_angular_velocity = 3.14
planner = MotionPlanner(max_linear_velocity, max_angular_velocity, dt)

print 'Running dynamic obstacles world'
while True:
    # collision testing
    if world.in_collision():
        print 'Collision'
        break

    # check if we have reached our goal
    vehicle_pos = vehicle.get_state()[:2]

    goal_distance = np.linalg.norm(vehicle_pos - goal)
    if goal_distance < goal_tolerance:
        print 'Goal Reached'
        break

    # if we want to update vehicle commands while running world
    linear_velocity, angular_velocity = planner.plan(world, goal, goal_tolerance)
    vehicle.set_commands(linear_velocity, angular_velocity)

    # update world
    world.update(dt)
    # draw world
    world.draw(screen)

# provide some time to view result
sleep(2.0)
