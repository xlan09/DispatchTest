'''
world snapshot example

showing ability to save world at a given state and play forward from
the snapshot state

this provides a view into future states of the world without
mangling the current state of the world.
'''

from world import World
from time import sleep

# timestep for world update
dt = 0.1

# create world
world = World()
# init pygame screen for visualization
screen = world.init_screen()
# populate obstacle field with dynamic obstacles
world.generate_obstacles(static=False)

# save a snapshot of the world
world_snapshot = world.get_snapshot()

print 'Viewing future states of the world'
for _ in xrange(0, 20):
    # update world
    world_snapshot.update(dt)
    # draw world
    world_snapshot.draw(screen)
    # wait dt amount of time
    sleep(dt)

sleep(1.0)

print 'Running world'
for _ in xrange(0, 20):
    # update world
    world.update(dt)
    # draw world
    world.draw(screen)
    # wait dt amount of time
    sleep(dt)

sleep(1.0)
