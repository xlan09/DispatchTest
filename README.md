Task:
    The goal of this coding challenge is to implement a motion planner that can guide a differential drive vehicle from an initial
    pose to a final position given the presence of moving obstacles in a fully
    observable world

    Adhere to the following vehicle constraints:
    - maximum linear velocity: 1.0 m/s
    - minimum linear velocity: -0.2 m/s
    - maximum angular velocity: 3.14 rad/s
    - minimum angular velocity: -3.14 rad/s
    - You have no constraints on accelerations

    Don't worry about goal orientation (theta), only goal position (x, y)

    Use a world timestep (dt) of 0.1 seconds

    You are welcome to use any libraries and additional tools to implement your
    algorithm. If you need to use C/C++ you are welcome to use ctypes
    (or any others) to wrap C/C++ code.

Deliverables:
    This will likely take a few hours, don't spend more than 6 hours.

    We aren't necessarily looking for a perfect or optimal solution, nor are
    we looking for a solution that runs in real-time, instead it is more
    important that you provide thorough and thoughtful discussion
    of the approach that you choose implement and well-written, clear code.

    - Provide a README
    -- Describe the approach you took and why you chose it
    -- Advantages and disadvantages
    -- Relevant materials, if used (books, papers, blogs, ...)
    -- Breakdown of time spent
    -- Future directions you would take if you had more time
    --- What would you do with a few days? with a few weeks?

    - Provide runnable code showing your implementation in action

Tools:
    You have been provided a World simulator via world.py. Examples of how
    to use world.py are provided via the following example files:
    - example_static_world.py
    - example_dynamic_world.py
    - example_snapshot.py
    - example_set_obstacles.py

Dependencies:
    - you will need the following:
    -- python2.7
    -- numpy
    -- pygame

    - if you are unfamiliar with numpy, we like this tutorial:
    -- https://docs.scipy.org/doc/numpy-dev/user/quickstart.html



# Gui
![Gui.png does not show properly, click Gui.png in Files to view](Gui.png)
