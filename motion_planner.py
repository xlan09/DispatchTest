#!/usr/bin/env python
"""
Motion planner to guide vehicle from start pos to goal pos in a dynamic env
"""

import numpy as np
import math
import logging
from timeit import default_timer as timer


class MotionPlanner(object):
    """
    Motion planner
    """

    def __init__(self, max_linear_vel, max_angular_vel, time_step):
        # I notice that you use protected atrributes, and use getter and setter, but I was taught to be pythonic, and
        # use public attributes as possible as we can, so here I will stick to my coding habits
        self.linear_vel = []
        self.angular_vel = []
        self.max_linear_vel = max_linear_vel
        self.max_angular_vel = max_angular_vel
        self.time_step = time_step
        self._init_vel(max_linear_vel, max_angular_vel)
        LogUtil.set_up_logging('MotionPlanner.txt')

    def _init_vel(self, max_linear_vel, max_angular_vel):
        """
        Discrete velocities
        """

        if max_linear_vel <= 0 or max_angular_vel <= 0:
            raise ValueError('Max velocity for planner must be positive')

        self.linear_vel = np.linspace(0.01, max_linear_vel, 10)
        self.angular_vel = np.linspace(-max_angular_vel, max_angular_vel, 20)

    def plan(self, world, goal_pos, goal_tolerance):
        """
        Do planning
        """

        try:
            return self._plan(world, goal_pos, goal_tolerance)
        except Exception as exp:
            # if planner does not work well, use current commands
            logging.error('Exceptions throw in motion planner, {}'.format(exp))

            return world.get_vehicle().get_curr_commands()

    def _plan(self, world, goal_pos, goal_tolerance):
        """
        Do planning
        :param world:
        :type: World
        :param goal_pos:
        :param goal_tolerance:
        :return:
        """

        goal_pos = np.array(goal_pos)
        x, y, theta = world.get_vehicle().get_state()
        vehicle_pos = np.array([x, y])

        dist2goal = np.linalg.norm(goal_pos - vehicle_pos)
        if dist2goal < goal_tolerance:
            return 0, 0

        # calculate preferred velocity
        preferred_velocity = (goal_pos - vehicle_pos) / dist2goal * self.max_linear_vel
        preferred_heading = math.atan2(preferred_velocity[1], preferred_velocity[0])
        if (theta <= 0 and preferred_heading <= 0) or (theta > 0 and preferred_heading > 0):
            preferred_angular_command = (preferred_heading - theta) / self.time_step
        else:
            heading_diff = preferred_heading - theta
            preferred_angular_command = heading_diff / self.time_step \
                if abs(heading_diff) < math.pi else ((-1.0) * heading_diff / abs(heading_diff)) \
                                                    * (2.0 * math.pi - abs(heading_diff)) / self.time_step
        # make sure preferred angular command is in feasible range
        if abs(preferred_angular_command) > self.max_angular_vel:
            preferred_angular_command = preferred_angular_command / abs(preferred_angular_command) * self.max_angular_vel

        neighbor_region_radius = np.amin(world.get_size()) / 2.0
        # neighbor_region_radius = 10 * world.get_obstacles()[0].get_radius() if world.get_obstacles() else 0.01
        obs_close_to_vehicle = self._find_obstacles_in_neighborhood(world, vehicle_pos, neighbor_region_radius)

        self._log_state_of_world([x, y, theta], obs_close_to_vehicle)

        # if no obstacle in neighborhood, apply preferred control
        if not obs_close_to_vehicle:
            preferred_linear_command = np.linalg.norm(preferred_velocity)
            logging.debug('No obstacle inside neighborhood, planned commands (linear, angular): {}'.format(
                [preferred_linear_command, preferred_angular_command]))
            return preferred_linear_command, preferred_angular_command

        # check whether preferred velocity is good
        preferred_linear_command = 0

        is_preferred_input_ok = False
        for linear_vec in self.linear_vel:
            velocity_vec = np.array([np.cos(preferred_heading), np.sin(preferred_heading)]) * linear_vec
            no_collision_with_any_obs = True
            for obs in obs_close_to_vehicle:
                if self._is_velocity_obstacle(vehicle_pos,
                                              velocity_vec,
                                              obs.get_velocity(),
                                              obs,
                                              neighbor_region_radius)[0]:
                    no_collision_with_any_obs = False
                    break

            if no_collision_with_any_obs:
                is_preferred_input_ok = True
                # always use maximum speed to move to goal
                if abs(preferred_linear_command) < abs(linear_vec):
                    preferred_linear_command = linear_vec

        if is_preferred_input_ok:
            logging.debug('Preferred input is found, planned commands (linear, angular): {}'
                         .format([preferred_linear_command, preferred_angular_command]))
            return preferred_linear_command, preferred_angular_command

        # preferred inputs are not good, have to search inputs
        admissive_inputs = {}
        infeasible_inputs = {}

        start = timer()
        for angular_vec in self.angular_vel:
            heading = theta + angular_vec * self.time_step
            for linear_vec in self.linear_vel:
                velocity_vec = np.array([np.cos(heading), np.sin(heading)]) * linear_vec
                no_collision_with_any_obs = True
                min_time_to_collision = float('inf')
                time_to_reach_neighborhood_perimeter = float('inf')
                for obs in obs_close_to_vehicle:
                    # start = timer(
                    is_obstacle, time = self._is_velocity_obstacle(vehicle_pos,
                                                                   velocity_vec,
                                                                   obs.get_velocity(),
                                                                   obs,
                                                                   neighbor_region_radius)
                    # logging.debug('Checking obstacle: {0}, angular_vec: {1}, linear_vec: {2} takes {3} seconds'
                    #              .format(obs.get_state(), angular_vec, linear_vec, timer() - start))
                    if is_obstacle:
                        no_collision_with_any_obs = False
                        min_time_to_collision = min(min_time_to_collision, time)
                    else:
                        # The time for non velocity obstacle is the time travelled to the neighborhood circle
                        time_to_reach_neighborhood_perimeter = min(time_to_reach_neighborhood_perimeter, time)

                if no_collision_with_any_obs:
                    admissive_inputs[(linear_vec, angular_vec)] = time_to_reach_neighborhood_perimeter
                else:
                    infeasible_inputs[(linear_vec, angular_vec)] = min_time_to_collision

        logging.debug('Calculating admissive inputs takes {0} seconds'.format(timer() - start))

        # if there are admissive inputs
        if admissive_inputs:
            # LogUtil.log_dict(admissive_inputs, 'Admissive Inputs')
            planned_input = self._find_lowest_penalty_input(admissive_inputs, preferred_velocity, theta)
            logging.debug('Admissive inputs found, planned commands (linear, angular): {}'.format(planned_input))
        else:
            # LogUtil.log_dict(infeasible_inputs, 'Infeasible Inputs')
            planned_input = self._find_lowest_penalty_input(infeasible_inputs, preferred_velocity, theta)
            logging.debug('Choosing in infeasible inputs, planned commands (linear, angular): {}'.format(planned_input))

        return planned_input

    def _find_lowest_penalty_input(self, inputs2collision_time, preferred_velocity, curr_vehicle_heading):
        """
        Find the input with the lowest penalty
        """

        lowest_penalty = float('inf')
        planned_input = (0, 0)
        for linear_vec, angular_vec in inputs2collision_time:
            heading = curr_vehicle_heading + angular_vec * self.time_step
            velocity_vec = np.array([np.cos(heading), np.sin(heading)]) * linear_vec

            penalty = 1.0 / inputs2collision_time[(linear_vec, angular_vec)] \
                      + np.linalg.norm(velocity_vec - np.array(preferred_velocity))

            if penalty < lowest_penalty:
                planned_input = (linear_vec, angular_vec)
                lowest_penalty = penalty

        return planned_input

    def _find_lowest_penalty_input_debug(self, inputs2collision_time, preferred_velocity, curr_vehicle_heading):
        """
            Find the input with the lowest penalty
        """

        penalties = {}
        for linear_vec, angular_vec in inputs2collision_time:
            heading = curr_vehicle_heading + angular_vec * self.time_step
            velocity_vec = np.array([np.cos(heading), np.sin(heading)]) * linear_vec

            penalty = 1.0 / inputs2collision_time[(linear_vec, angular_vec)] \
                      + np.linalg.norm(velocity_vec - np.array(preferred_velocity))
            penalties[(linear_vec, angular_vec)] = penalty

        sorted_penalties = sorted(penalties, key= lambda x: penalties[x])

        logging.debug('--------------Print penalties------------------')
        for key_iter in sorted_penalties:
            logging.debug('Penalty: {}, {}'.format(key_iter, penalties[key_iter]))

        return sorted_penalties[0]

    def _is_velocity_obstacle(self, vehicle_pos, velocity_vec, obs_velocity_vec, obstacle, neighbor_region_radius):
        """
        Check if a velocity of the vehicle is velocity obstacle
        :param obstacle:
        :type: Obstacle
        :return:
        """

        velocity_vec = np.array(velocity_vec)
        obs_velocity_vec = np.array(obs_velocity_vec)
        vehicle_pos = np.array(vehicle_pos)

        relative_velocity = velocity_vec - obs_velocity_vec
        relative_velocity_magnitude = np.linalg.norm(relative_velocity)
        if relative_velocity_magnitude < 10 ** (-8):
            return False, float('inf')
        else:
            collision_radius = 2.0 * obstacle.get_radius()
            if self._is_collision(vehicle_pos, relative_velocity, obstacle, collision_radius):
                is_velocity_obs = True
                dist2obs = np.linalg.norm(np.array(obstacle.get_state()) - vehicle_pos)

                # since we used collision radius, the vehicle may stay at the cushion region of obs
                time = (dist2obs - collision_radius) / relative_velocity_magnitude if dist2obs > collision_radius \
                    else (dist2obs - obstacle.get_radius()) / relative_velocity_magnitude
            else:
                is_velocity_obs = False
                time = neighbor_region_radius / relative_velocity_magnitude

            return is_velocity_obs, time

    def _is_collision(self, vehicle_pos, relative_velocity, obstacle, collision_radius):
        """
        Check whether the vehicle will collide with obs
        """

        if obstacle.in_collision(vehicle_pos, collision_radius):
            return True

        relative_velocity_heading = math.atan2(relative_velocity[1], relative_velocity[0])

        vec_to_obs = np.array(obstacle.get_state()) - np.array(vehicle_pos)
        vec_to_obs_heading = math.atan2(vec_to_obs[1], vec_to_obs[0])
        tangent_angle = math.asin(collision_radius / np.linalg.norm(vec_to_obs))

        return vec_to_obs_heading - tangent_angle <= relative_velocity_heading <= vec_to_obs_heading + tangent_angle

    def _find_obstacles_in_neighborhood(self, world, vehicle_pos, neighborhood_radius):
        """
        Only consider obstacles near the vehicle
        :param world:
        :type: World
        """

        res = []
        for obs in world.get_obstacles():
            if np.linalg.norm(np.array(obs.get_state()) - np.array(vehicle_pos)) <= neighborhood_radius:
                res.append(obs)

        return res

    def _log_state_of_world(self, vehicle_pose, obs_in_neighborhood):
        """
        Log state of world
        """

        logging.debug('------------------------Logging State of World--------------------')
        x, y, theta = vehicle_pose
        logging.debug('vehicle pos: {0}, heading: {1}'.format([x, y], theta))
        logging.debug('Obstacles in neighborhood:')
        for obs in obs_in_neighborhood:
            logging.debug('Obstacle, pos: {}, heading: {}'.format(obs.get_state(), obs.get_direction()))


class LogUtil(object):
    """
    Log util
    """

    @staticmethod
    def set_up_logging(log_file_name, logging_level=logging.DEBUG):
        logging.basicConfig(filename=log_file_name, filemode='w', level=logging_level,
                            format='%(asctime)s %(message)s')
        # logging.getLogger().addHandler(logging.StreamHandler())

    @staticmethod
    def log_dict(one_dict, dict_info):
        logging.debug('--------------Printing {}-----------------'.format(dict_info))
        for k in one_dict:
            logging.debug('key: {}, value: {}'.format(k, one_dict[k]))