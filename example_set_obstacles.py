'''
example showing how to manually intialize obstacles
'''

from world import World, Obstacle
import numpy as np
from time import sleep

# create world
world = World()
# init pygame screen for visualization
screen = world.init_screen()

# get world_size -- passed to obstacles
world_size = world.get_size()

# number of obstacles
n_obstacles = 5

# obstacle positions horizontal across screen
xs = np.linspace(1.0, 4.0, n_obstacles)
ys = np.ones(n_obstacles) * 2.0

# direction pointing downwards on the screen
directions = np.ones(n_obstacles) * np.pi/2.0
# velocity of 0.25 m/s
velocities = np.ones(n_obstacles) * 0.25

obstacles = []
for i in xrange(0, n_obstacles):
    obstacle = Obstacle(world_size)
    # override default obstacle state
    obstacle.set_state(
        [xs[i], ys[i]],
        directions[i],
        velocities[i]
    )
    obstacles.append(obstacle)

# set obstacles from our obstacle list instead of randomly generating
world.set_obstacles(obstacles)

print 'Running world'
dt = 0.1
for _ in xrange(0, 50):
    # update world
    world.update(dt)
    # draw world
    world.draw(screen)
    # wait dt amount of time
    sleep(dt)

sleep(1.0)
